# I'm Hungry
Find places to eat now :D

## This project was built using:
  * React
  * Typescript
  * Apollo
  * React Testing Library
  * Emotion
  * Eslint + Prettier

## Code:
Please [check the DEV file](DEV.md)

## How to run it:
1. ```npm install``` ;)
2. ```npm start```

## Folder Structure:
```
project
│   README.md
│   DEV.md
└───public
└───src
│   │
│   └───components
│   |   └──common.tsx
│   |       └───SharedComponent.tsx
│   └───pages
│   |   └───RoutedComponent.tsx
│   └───utils
│   └───theme
```

1. **Components Folder**  (All React Components)

    1.1 **Commom Folder** (Components that are shared across the app, it is also the base of our "Design System")

2.  **Pages Folder** (Only Routed Components. _routed components are the owns that are directly children of Browser Router and are handled by a Router component_)

3. **Utils Folder** (Javascript files used to handle general cases, like acessing convert a hex string to rgba)

4. **Theme** (Our themes, and it's setup, like the ThemeProvider, it's own custom hook, media queries, etc)


## Create React App Default Doc:
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
