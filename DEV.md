# Development Guide

## How to start working on this repository?

1. Create your own branch from **master** branch with this nomenclature.

2. Name your own branch like this: **feature_DESCRIPTION**. Always start your new branch name with **feature\_** and make a small description of it.

3. Work on your own branch. Run tests and linters before commiting on your branch.

4. Send a pull request (PR) to **development** branch. Add a description of your PR including links to Trello's cards.

5. Good Hacking

## Code Quality Assurance

_Before creating your commit, be sure that your code is clean and follow community best practices and our own project best practices._

1. If you're creating/modifying a component, make the tests for it.

2. Run ESLint: **npm run lint** before doing your commit.

3. Run Tests: **npm test** before doing your commit.

4. Make your commit if everything is ok.
