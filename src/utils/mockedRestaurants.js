/* eslint-disable @typescript-eslint/camelcase */
export default {
  search: {
    business: [
      {
        name: 'The Hairy Pig Deli',
        url:
          'https://www.yelp.com/biz/the-hairy-pig-deli-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media1.fl.yelpcdn.com/bphoto/W9a2CsO3livV1o7exRckvw/o.jpg'
        ]
      },
      {
        name: 'Bakfickan',
        url:
          'https://www.yelp.com/biz/bakfickan-stockholm-2?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media2.fl.yelpcdn.com/bphoto/APTUmXwgdMWpjXVqsq_2WA/o.jpg'
        ]
      },
      {
        name: 'Meatballs - For the People',
        url:
          'https://www.yelp.com/biz/meatballs-for-the-people-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4,
        photos: [
          'https://s3-media3.fl.yelpcdn.com/bphoto/tjgkPRRlK4NZiO5TBcsPUA/o.jpg'
        ]
      },
      {
        name: 'Rolfs Kök',
        url:
          'https://www.yelp.com/biz/rolfs-k%C3%B6k-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media2.fl.yelpcdn.com/bphoto/4qS-xmgKP_CiVGBs9C8Dyw/o.jpg'
        ]
      },
      {
        name: 'Vete-Katten',
        url:
          'https://www.yelp.com/biz/vete-katten-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4,
        photos: [
          'https://s3-media4.fl.yelpcdn.com/bphoto/uXXYum3YjU_oPt8Z9kgKpQ/o.jpg'
        ]
      },
      {
        name: 'Fotografiska',
        url:
          'https://www.yelp.com/biz/fotografiska-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4,
        photos: [
          'https://s3-media2.fl.yelpcdn.com/bphoto/Tr8mGfemkUtAkkoBUSs0ZQ/o.jpg'
        ]
      },
      {
        name: 'Lilla Ego',
        url:
          'https://www.yelp.com/biz/lilla-ego-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media3.fl.yelpcdn.com/bphoto/itHPlrloihB5ftrqQQN-TA/o.jpg'
        ]
      },
      {
        name: 'Strömmingsvagnen',
        url:
          'https://www.yelp.com/biz/str%C3%B6mmingsvagnen-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media2.fl.yelpcdn.com/bphoto/Y6hL3OCuGjzmMHv1stkQpQ/o.jpg'
        ]
      },
      {
        name: 'Johan & Nyström Konceptbutik',
        url:
          'https://www.yelp.com/biz/johan-och-nystr%C3%B6m-konceptbutik-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media2.fl.yelpcdn.com/bphoto/2Q-Ui1_JICRrnCsbCr5WIw/o.jpg'
        ]
      },
      {
        name: 'Hermans',
        url:
          'https://www.yelp.com/biz/hermans-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4,
        photos: [
          'https://s3-media3.fl.yelpcdn.com/bphoto/jtVBt1MRmY8bxeR75AmFfA/o.jpg'
        ]
      },
      {
        name: 'Cafe Pascal',
        url:
          'https://www.yelp.com/biz/cafe-pascal-stockholm-2?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media2.fl.yelpcdn.com/bphoto/_eVuwPAPFEPoZH21c0X57w/o.jpg'
        ]
      },
      {
        name: 'Riche',
        url:
          'https://www.yelp.com/biz/riche-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4,
        photos: [
          'https://s3-media2.fl.yelpcdn.com/bphoto/QYLgV3eRqvUnVBPkCesyxw/o.jpg'
        ]
      },
      {
        name: 'Tradition',
        url:
          'https://www.yelp.com/biz/tradition-stockholm-4?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media2.fl.yelpcdn.com/bphoto/B8iyP2eg_GNAa9qRTFmZ6Q/o.jpg'
        ]
      },
      {
        name: 'Mathias Dahlgren Matbaren',
        url:
          'https://www.yelp.com/biz/matbaren-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media3.fl.yelpcdn.com/bphoto/OJmS-93HdjSioBKRIXdWAg/o.jpg'
        ]
      },
      {
        name: 'Pharmarium',
        url:
          'https://www.yelp.com/biz/pharmarium-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4,
        photos: [
          'https://s3-media2.fl.yelpcdn.com/bphoto/Y7ofdI3zNyR-VfY7RbWbhQ/o.jpg'
        ]
      },
      {
        name: 'Sushi Sho',
        url:
          'https://www.yelp.com/biz/sushi-sho-stockholm-2?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media1.fl.yelpcdn.com/bphoto/wgf_-H_UV3cpzWsd_c06zA/o.jpg'
        ]
      },
      {
        name: "Flippin' Burgers",
        url:
          'https://www.yelp.com/biz/flippin-burgers-stockholm-2?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4,
        photos: [
          'https://s3-media3.fl.yelpcdn.com/bphoto/UFe581S5lmbzEtoVek3mrA/o.jpg'
        ]
      },
      {
        name: 'Blå Dörren',
        url:
          'https://www.yelp.com/biz/bl%C3%A5-d%C3%B6rren-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4,
        photos: [
          'https://s3-media2.fl.yelpcdn.com/bphoto/UOSD_cdeKPtKhUerL3eu3w/o.jpg'
        ]
      },
      {
        name: 'Älskade Traditioner',
        url:
          'https://www.yelp.com/biz/%C3%A4lskade-traditioner-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media1.fl.yelpcdn.com/bphoto/IZWA5znNhv927fMczDFUZg/o.jpg'
        ]
      },
      {
        name: 'Kryp In',
        url:
          'https://www.yelp.com/biz/kryp-in-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
        rating: 4.5,
        photos: [
          'https://s3-media4.fl.yelpcdn.com/bphoto/VfL_1pyy2r3lDGOQBJPkoQ/o.jpg'
        ]
      }
    ]
  }
};
