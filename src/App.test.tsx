// app.test.js
import React from 'react';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { render } from '@testing-library/react';
import { MockedProvider } from '@apollo/react-testing';
import Routes from './Routes';
import { ThemeProvider } from './theme/ThemeProider';

test('full app rendering/navigating', () => {
  const history = createMemoryHistory();
  const { container } = render(
    <MockedProvider>
      <ThemeProvider>
        <Router history={history}>
          <Routes />
        </Router>
      </ThemeProvider>
    </MockedProvider>
  );
  expect(container.innerHTML).toMatch('Find a place to eat now :D');
  history.push('/restaurant/123');
  expect(container.innerHTML).toMatch('Restaurant id: 123');
});
