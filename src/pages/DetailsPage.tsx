import React from 'react';
import { useParams } from 'react-router-dom';
import Row from '../components/common/Row';
import Col from '../components/common/Col';

const DetailsPage: React.FC = () => {
  const { id } = useParams();
  return (
    <Row>
      <Col>
        <h1> Restaurant id: {id}</h1>
      </Col>
    </Row>
  );
};

export default DetailsPage;
