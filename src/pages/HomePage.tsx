import React from 'react';
import Row from 'components/common/Row';
import Restaurants from 'components/Restaurants';
import SearchBox from 'components/SearchBox';
import styled from 'theme';

const Subtitle = styled('h2')`
  color: ${({ theme }): string => theme.text.secondary};
`;

const HomePage: React.FC = () => {
  const [place, setPlace] = React.useState<string>('Stockholm');

  const updatePlace = (newPlace: string): void => setPlace(newPlace);

  return (
    <>
      <Row justify="center">
        <Subtitle>Find a place to eat now :D</Subtitle>
      </Row>
      <SearchBox place={place} updatePlace={updatePlace} />
      <Restaurants place={place} />
    </>
  );
};

export default HomePage;
