import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from 'theme';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from 'apollo-boost';
import Header from 'components/Header';
import Routes from 'Routes';

const client = new ApolloClient({
  uri: 'https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/graphql',
  headers: {
    Authorization: `Bearer ${process.env.REACT_APP_API_KEY}`,
    'Accept-Language': 'en_US'
  }
});

const App: React.FC = () => {
  return (
    <ThemeProvider>
      <Header />
      <ApolloProvider client={client}>
        <BrowserRouter basename={process.env.PUBLIC_URL}>
          <Routes />
        </BrowserRouter>
      </ApolloProvider>
    </ThemeProvider>
  );
};

export default App;
