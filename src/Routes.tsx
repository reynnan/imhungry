import React from 'react';
import { Route, Switch } from 'react-router-dom';
import HomePage from 'pages/HomePage';
import RestaurantPage from 'pages/DetailsPage';

const Routes: React.FC = () => {
  return (
    <Switch>
      <Route exact path="/">
        <HomePage />
      </Route>
      <Route path="/restaurant/:id">
        <RestaurantPage />
      </Route>
    </Switch>
  );
};

export default Routes;
