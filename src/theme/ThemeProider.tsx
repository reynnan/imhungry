import React, { useEffect } from 'react';
import { Global, css, SerializedStyles } from '@emotion/core';
import { ThemeProvider as EmotionTheme } from 'emotion-theming';

import relaxed from './relaxedTheme';
import cloudy from './cloudyTheme';
import { ThemeUpdaterProps, AvailableThemesTypes, ThemeNames } from './types';

export const availableThemes: AvailableThemesTypes = {
  relaxed,
  cloudy
};

const ThemeUpdater = React.createContext({} as ThemeUpdaterProps);

const THEME_KEY = 'theme';
const DEFAULT_THEME: ThemeNames = 'relaxed';

export const ThemeProvider: React.FC<React.ReactNode> = ({ children }) => {
  const [theme, setTheme] = React.useState<ThemeNames>('relaxed');
  const updateTheme = (themeName: ThemeNames): void => {
    setTheme(themeName);
    localStorage.setItem(THEME_KEY, themeName);
  };

  useEffect(() => {
    const storedTheme = localStorage.getItem(THEME_KEY) as ThemeNames | null;
    setTheme(storedTheme || DEFAULT_THEME);
  }, [setTheme]);
  return (
    <EmotionTheme theme={availableThemes[theme]}>
      <ThemeUpdater.Provider
        value={{
          theme,
          updateTheme
        }}
      >
        <Global
          styles={(emotionTheme): SerializedStyles => css`
            * {
              transition: background-color 0.5s ease-in-out;
            }
            body {
              font-family: 'Spartan', serif;
              margin: 0;
              padding: 0;
              min-height: 100vh;
              max-width: 100vw;
              background-color: ${emotionTheme.secondary.light};
            }
            button,
            input {
              font-family: inherit;
              font-size: 100%;
              line-height: 1.15;
              margin: 0;
            }
          `}
        />
        {children}
      </ThemeUpdater.Provider>
    </EmotionTheme>
  );
};

export const useThemeUpdater = (): ThemeUpdaterProps =>
  React.useContext(ThemeUpdater);
