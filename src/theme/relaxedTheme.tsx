import { ThemeOptions } from './types';

const relaxed: ThemeOptions = {
  primary: {
    main: '#d81b60',
    light: '#ff5c8d',
    dark: '#a00037'
  },
  secondary: {
    main: '#fafafa',
    light: '#ffffff',
    dark: '#c7c7c7'
  },
  text: {
    primary: '#ffffff',
    secondary: '#000000',
    input: '#000000'
  }
};

export default relaxed;
