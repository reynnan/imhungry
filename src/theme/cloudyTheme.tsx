import { ThemeOptions } from './types';

const cloudy: ThemeOptions = {
  primary: {
    main: '#533638',
    light: '#806062',
    dark: '#291012'
  },
  secondary: {
    main: '#455a64',
    light: '#718792',
    dark: '#1c313a'
  },
  text: {
    primary: '#ffffff',
    secondary: '#ffffff',
    input: '#000000'
  }
};

export default cloudy;
