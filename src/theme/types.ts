import styled, { CreateStyled } from '@emotion/styled';

type ColorBase = {
  main: string;
  light: string;
  dark: string;
};

export type ThemeOptions = {
  primary: ColorBase;
  secondary: ColorBase;
  text: {
    primary: string;
    secondary: string;
    input: string;
  };
};

export type ThemeNames = 'relaxed' | 'cloudy';

export type AvailableThemesTypes = {
  relaxed: ThemeOptions;
  cloudy: ThemeOptions;
};

export type ThemeUpdaterProps = {
  updateTheme: (themeName: ThemeNames) => void;
  theme: ThemeNames;
};

export default styled as CreateStyled<ThemeOptions>;
