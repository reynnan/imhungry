import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import styled from './types';
import { ThemeProvider, useThemeUpdater } from './ThemeProider';
import '@testing-library/jest-dom/extend-expect';

// this is our default background primary color, and it will be used to test the ThemeProvider
const RGB_DEFAULT_THEME_COLOR = 'rgb(255, 92, 141)';

afterEach(cleanup);

test('should apply global styles to children', () => {
  const { getByTestId } = render(
    <ThemeProvider>
      <button type="button" data-testid="button-test">
        Just a Button
      </button>
    </ThemeProvider>
  );

  const style = window.getComputedStyle(getByTestId('button-test'));
  // line height was applied using Emotion's Global Component
  expect(style.lineHeight).toBe('1.15');
});

test('should be able to access theme props from children', () => {
  const StyledButton = styled('button')`
    color: ${(props): string => props.theme.primary.light};
  `;
  const { getByTestId } = render(
    <ThemeProvider>
      <StyledButton data-testid="button-test"> Button </StyledButton>
    </ThemeProvider>
  );

  const style = window.getComputedStyle(getByTestId('button-test'));
  expect(style.color).toBe(RGB_DEFAULT_THEME_COLOR);
});

test('should be able to update theme using useThemeUpdater', () => {
  const TestHook: React.FC = () => {
    const themeUpdater = useThemeUpdater();
    const TestDiv = styled('div')`
      background-color: ${(props): string => props.theme.primary.light};
    `;
    return (
      <>
        <TestDiv data-testid="test-div" />
        <button
          data-testid="button"
          type="button"
          onClick={(): void => themeUpdater('cloudy')}
        >
          Update Theme
        </button>
      </>
    );
  };

  const { getByTestId } = render(
    <ThemeProvider>
      <TestHook />
    </ThemeProvider>
  );

  expect(getByTestId('test-div')).toHaveStyle(
    `background-color: ${RGB_DEFAULT_THEME_COLOR}`
  );
  fireEvent.click(getByTestId('button'));
});
