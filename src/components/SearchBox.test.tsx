// app.test.js
import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import { ThemeProvider } from 'theme';
import SearchBox from './SearchBox';

afterEach(cleanup);

const place = 'Stockholm';
const inputTestid = 'input-location';

const updatePlace = (newPlace: string): void => {};

test('Renders searchbox elements with default values', () => {
  const { container, getByTestId } = render(
    <ThemeProvider>
      <SearchBox place={place} updatePlace={updatePlace} />
    </ThemeProvider>
  );
  // checking if button is rendered
  expect(container.innerHTML).toMatch('Search');
  const input = getByTestId(inputTestid) as HTMLInputElement;
  expect(input.value).toBe('Stockholm');
});

test('Input state updates onChange', () => {
  const { getByTestId } = render(
    <ThemeProvider>
      <SearchBox place={place} updatePlace={updatePlace} />
    </ThemeProvider>
  );

  const input = getByTestId(inputTestid) as HTMLInputElement;
  // expect input to not have change before call on change
  expect(input.value).toBe('Stockholm');
  fireEvent.change(input, { target: { value: 'Brazil!' } });
  expect(input.value).toBe('Brazil!');
});

test('Should have error message when submit empty value', () => {
  const { getByTestId, container } = render(
    <ThemeProvider>
      <SearchBox place="" updatePlace={updatePlace} />
    </ThemeProvider>
  );

  const form = getByTestId('search-form') as HTMLInputElement;
  fireEvent.submit(form);
  expect(container.innerHTML).toMatch('Input field should have an value');
});

test('Error message should disappear when sending text with value', () => {
  const { queryByTestId, getByTestId, container } = render(
    <ThemeProvider>
      <SearchBox place="" updatePlace={updatePlace} />
    </ThemeProvider>
  );

  // creating the error
  const form = getByTestId('search-form') as HTMLInputElement;
  fireEvent.submit(form);
  expect(container.innerHTML).toMatch('Input field should have an value');
  // fixing it
  const input = getByTestId(inputTestid) as HTMLInputElement;
  fireEvent.change(input, { target: { value: 'São Paulo' } });
  fireEvent.submit(form);
  expect(queryByTestId(/error-message/)).toBeNull();
});
