import styled from '@emotion/styled';

type StyledColProps = {
  grow?: number;
  align?:
    | 'auto'
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'baseline'
    | 'stretch';
  basisPercentage?: number;
};

const Col = styled('div')<StyledColProps>`
  flex-grow: ${({ grow = 0 }): number => grow};
  ${({ align = '' }): string =>
    align === 'center'
      ? 'align-self: center; text-align: center;'
      : `align-self: ${align};`}
  ${({ basisPercentage = '' }): string =>
    basisPercentage ? `flex-basis: ${basisPercentage}%` : ``}
`;

export default Col;
