import React from 'react';
import styled from 'theme';

type ButtonWrapperProps = {
  secondary?: boolean;
  disabled?: boolean;
  centerContent?: boolean;
  round?: boolean;
  gradientBg?: Array<string>;
  active?: boolean;
  children?: React.ReactNode;
};

const ButtonWrapper = styled('button')<ButtonWrapperProps>`
  display: flex;
  align-items: center;
  font-size: 1.2em;
  font-weight: 400;
  border: none;
  outline: 0;
  width: 100%;
  height: 100%;
  min-height: 40px;
  background-position: center;
  ${(props): string => (props.round ? 'border-radius: 50%;' : '')}
  padding: ${(props): string => (props.round ? '18px' : '0')};
  justify-content: ${(props): string =>
    props.centerContent ? 'center' : 'initial'};
  cursor: ${(props): string => (props.disabled ? 'not-allowed' : 'pointer')};
  ${(props): string => {
    if (props.gradientBg && props.gradientBg?.length > 0) {
      const [firstColor, secondColor] = props.gradientBg;
      return `background: linear-gradient(90deg, ${firstColor} 10%, ${secondColor} 93%);`;
    }
    return `background-color: ${
      props.secondary ? props.theme.secondary.main : props.theme.primary.main
    };`;
  }}
  color: ${(props): string =>
    props.secondary ? props.theme.text.secondary : props.theme.text.primary};
  &:hover {
    background-color: ${(props): string =>
      props.secondary ? props.theme.secondary.dark : props.theme.primary.dark};
  }
  &:active {
    background-color: ${(props): string =>
      props.secondary
        ? props.theme.secondary.light
        : props.theme.primary.light};
    background-size: 100%;
    transition: background 0s;
  }
  ${(props): string => {
    if (props.active) {
      return `box-shadow: 0px 0px 0px 1px ${
        props.secondary
          ? props.theme.secondary.light
          : props.theme.primary.light
      };`;
    }
    return '';
  }}
`;

type ButtonProps = {
  onClick?: () => void;
  id?: string;
  type?: 'button' | 'reset' | 'submit';
};

const Button: React.FC<ButtonProps & ButtonWrapperProps> = ({
  onClick,
  id,
  secondary,
  disabled,
  centerContent,
  round,
  gradientBg,
  active,
  type,
  children
}) => {
  return (
    <ButtonWrapper
      id={id}
      data-testid={id}
      type={type}
      onClick={onClick}
      secondary={secondary}
      disabled={disabled}
      round={round}
      gradientBg={gradientBg}
      centerContent={centerContent}
      active={active}
    >
      {children}
    </ButtonWrapper>
  );
};

export default Button;
