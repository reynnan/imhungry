import styled from '@emotion/styled';

type HiddenProps = {
  active?: boolean;
};

const Hidden = styled('div')<HiddenProps>`
  ${(props): string => (!props.active ? 'display: none' : '')}
`;

export default Hidden;
