import styled from 'theme';

const Input = styled('input')`
  margin: 0;
  outline: none;
  border: none;
  width: 100%;
  padding: 15px;
  box-sizing: border-box;
  font-weight: 300;
  margin-right: 1%;
  color: ${(props): string => props.theme.text.input};
  border: 2px solid ${(props): string => props.theme.secondary.dark};
  &::placeholder {
    font-weight: 200;
  }
`;

export default Input;
