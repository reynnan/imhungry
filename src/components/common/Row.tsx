import styled from '@emotion/styled';

type Row = {
  direction?: 'row' | 'column';
  justify?:
    | 'flex-start'
    | 'center'
    | 'flex-end'
    | 'space-between'
    | 'space-around'
    | 'space-evenly';
  alignContent?:
    | 'stretch'
    | 'center'
    | 'flex-start'
    | 'flex-end'
    | 'space-between'
    | 'space-around';
  alignItems?: 'flex-start' | 'center' | 'flex-end' | 'stretch' | 'baseline';
};

const Row = styled('div')<Row>`
  display: flex;
  flex-wrap: wrap;
  flex-direction: ${(props): string =>
    props.direction ? `${props.direction}` : 'row'};
  justify-content: ${(props): string =>
    props.justify ? props.justify : 'flex-start'};
  align-content: ${(props): string =>
    props.alignContent ? props.alignContent : 'flex-start'};
  align-items: ${(props): string => {
    return props.alignItems ? props.alignItems : 'stretch';
  }};
`;

export default Row;
