import React from 'react';
import styled, { mq } from 'theme';
import Row from './common/Row';
import Col from './common/Col';
import Input from './common/Input';
import Button from './common/Button';

type SearchBoxProps = {
  place: string;
  updatePlace: (newPlace: string) => void;
};

const SearchBoxRow = styled('div')`
  padding: 1.5%;
  box-sizing: border-box;
  border: 0.5px solid ${(props): string => props.theme.primary.main};
  margin-right: 15%;
  margin-left: 15%;
  margin-bottom: 5%;
`;

const InputWrapper = styled(Col)`
  ${mq[0]} {
    margin-right: 0;
  }
  ${mq[1]} {
    margin-right: 1%;
  }
  ${mq[2]} {
    margin-right: 2%;
  }
  ${mq[3]} {
    margin-right: 3%;
  }
`;

const ErrorMessage = styled('p')`
  color: red;
`;

const SearchBox: React.FC<SearchBoxProps> = ({ place, updatePlace }) => {
  const [search, setSearch] = React.useState<string>(place);
  const [error, setError] = React.useState<boolean>(false);

  const handleSubmit = (e: React.SyntheticEvent): void => {
    e.preventDefault();
    if (search !== '') {
      updatePlace(search);
      setError(false);
    } else {
      setError(true);
    }
  };

  return (
    <form data-testid="search-form" onSubmit={handleSubmit}>
      <SearchBoxRow>
        <Row>
          <InputWrapper grow={10}>
            <Input
              data-testid="input-location"
              value={search}
              onChange={
                (e: React.ChangeEvent<HTMLInputElement>): void =>
                  setSearch(e.target.value)
                // eslint-disable-next-line react/jsx-curly-newline
              }
            />
          </InputWrapper>
          <Col grow={2}>
            <Button type="submit" centerContent>
              Search
            </Button>
          </Col>
        </Row>
        {error && (
          <Row>
            <ErrorMessage data-testid="error-message">
              Input field should have an value
            </ErrorMessage>
          </Row>
        )}
      </SearchBoxRow>
    </form>
  );
};

export default SearchBox;
