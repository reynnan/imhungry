// app.test.js
import React from 'react';
import { render, wait, cleanup } from '@testing-library/react';
import { MockedProvider } from '@apollo/react-testing';
import { ThemeProvider } from 'theme';
import { act } from 'react-dom/test-utils';
import Restaurants, { RESTAURANTS_QUERY } from './Restaurants';

afterEach(cleanup);

const place = 'Lund';

const mocks = [
  {
    request: {
      query: RESTAURANTS_QUERY,
      variables: {
        name: 'Buck'
      }
    },
    result: {
      data: {
        search: {
          business: [
            {
              name: 'The Hairy Pig Deli',
              url:
                'https://www.yelp.com/biz/the-hairy-pig-deli-stockholm?adjust_creative=8pHJO_ZpBKpHOAlY5R09Ew&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=8pHJO_ZpBKpHOAlY5R09Ew',
              rating: 4.5,
              photos: [
                'https://s3-media1.fl.yelpcdn.com/bphoto/W9a2CsO3livV1o7exRckvw/o.jpg'
              ]
            }
          ]
        }
      }
    }
  }
];

const RestaurantTextComponent: React.FC = () => {
  return (
    <ThemeProvider>
      <MockedProvider mocks={mocks} addTypename={false}>
        <Restaurants place={place} />
      </MockedProvider>
    </ThemeProvider>
  );
};

it('should render loading state initialy', async () => {
  let container: HTMLElement;
  act(() => {
    container = render(<RestaurantTextComponent />).container;
    expect(container.innerHTML).toMatch(' Loading... ');
  });
});

it('should render restaurants', async () => {
  let container: HTMLElement;
  act(() => {
    container = render(<RestaurantTextComponent />).container;
  });
  await wait(() => expect(container.innerHTML).toMatch('The Hairy Pig Deli'));
});
