import React from 'react';
import styled, { mq } from 'theme';

type CardProps = {
  title: string;
  background: string;
  subtitle: string;
  link: string;
};

const Container = styled('div')`
  position: relative;
  margin: auto;
  overflow: hidden;
`;

const Link = styled('a')`
  text-decoration: none;
`;

const Background = styled('img')`
  width: 100%;
  transition: transform 0.5s, filter 0.2s ease-in-out;
  ${mq[0]} {
    height: auto;
  }
  ${mq[3]} {
    max-height: 300px;
    filter: blur(2px);
  }
  &:hover {
    filter: blur(0);
    transform: scale(1.5);
  }
`;

const Title = styled('h4')`
  position: absolute;
  top: 8px;
  left: 16px;
  color: ${(props): string => props.theme.text.primary};
`;

const Subtitle = styled('h4')`
  color: ${(props): string => props.theme.text.primary};
  position: absolute;
  bottom: 8px;
  left: 16px;
`;

const SearchBox: React.FC<CardProps> = ({
  title,
  background,
  subtitle,
  link
}) => {
  return (
    <Container>
      <Link href={link} target="_blank">
        <Background src={background} />
        <Title> {title} </Title>
        <Subtitle>Rating: {subtitle}</Subtitle>
      </Link>
    </Container>
  );
};

export default SearchBox;
