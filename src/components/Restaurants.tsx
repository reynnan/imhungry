import React from 'react';
import styled, { mq } from 'theme';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import mockedRestaurants from 'utils/mockedRestaurants';
import Row from './common/Row';
import Card from './Card';

type Restaurant = {
  name: string;
  url: string;
  rating: number;
  photos: Array<string>;
};

export const RESTAURANTS_QUERY = gql`
  query search($location: String!) {
    search(location: $location, open_now: true) {
      business {
        name
        url
        rating
        photos
      }
    }
  }
`;

const ImageContainer = styled('div')`
  line-height: 0;
  column-gap: 0px;
  ${mq[0]} {
    column-count: 1;
  }
  ${mq[1]} {
    column-count: 2;
  }
  ${mq[2]} {
    column-count: 3;
  }
  ${mq[3]} {
    column-count: 4;
  }
`;

const StyledH3 = styled('h3')`
  color: ${({ theme }): string => theme.text.secondary};
`;

const renderCard = (restaurant: Restaurant): React.ReactNode => (
  <Card
    key={restaurant.url}
    background={restaurant.photos[0]}
    link={restaurant.url}
    title={restaurant.name}
    subtitle={`${restaurant.rating}`}
  />
);

const Restaurants: React.FC<{ place: string }> = ({ place }) => {
  const { loading, error, data } = useQuery(RESTAURANTS_QUERY, {
    variables: { location: place }
  });

  return (
    <>
      <Row style={{ padding: '0 15px' }}>
        <StyledH3>
          {loading ? 'Fetching' : 'Showing'} places at {place}
        </StyledH3>
      </Row>
      {!error && loading && <Row justify="center"> Loading... </Row>}
      <ImageContainer>
        {error && !loading && mockedRestaurants.search.business.map(renderCard)}
        {!error && !loading && data.search.business.map(renderCard)}
      </ImageContainer>
    </>
  );
};

export default Restaurants;
