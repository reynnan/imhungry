import React from 'react';
import styled, { useThemeUpdater, availableThemes } from 'theme';
import { ThemeNames } from 'theme/types';
import Row from './common/Row';
import Col from './common/Col';
import Button from './common/Button';
import Hidden from './common/Hidden';

const HeaderWrapper = styled('nav')`
  background-color: ${(props): string => props.theme.primary.main};
  padding: 1%;
  margin-bottom: 5%;
`;

const Title = styled('h1')`
  color: ${(props): string => props.theme.text.secondary};
`;

const Header: React.FC = () => {
  const { updateTheme, theme } = useThemeUpdater();

  const handleThemeUpdate = (themeName: ThemeNames): void => {
    updateTheme(themeName);

  };

  return (
    <HeaderWrapper>
      <Row>
        <Col grow={11}>
          <Title> Im Hungry </Title>
        </Col>
        <Col align="center">
          {theme === 'relaxed' && (
            <Button
              id="cloudy-theme-button"
              gradientBg={[
                availableThemes.cloudy.primary.main,
                availableThemes.cloudy.secondary.main
              ]}
              round
              secondary
              onClick={(): void => handleThemeUpdate('cloudy')}
              data-testid="cloudy-theme-button"
            />
          )}

          {theme === 'cloudy' && (
            <Button
              id="relaxed-theme-button"
              gradientBg={[
                availableThemes.relaxed.primary.main,
                availableThemes.relaxed.secondary.main
              ]}
              round
              secondary
              onClick={(): void => handleThemeUpdate('relaxed')}
            />
          )}
        </Col>
      </Row>

      <Hidden data-testid="current-theme">{theme}</Hidden>
    </HeaderWrapper>
  );
};

export default Header;
