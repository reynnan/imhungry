// app.test.js
import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import { ThemeProvider } from 'theme';
import Header from './Header';

afterEach(cleanup);

test('Header renders app name', () => {
  const { container } = render(
    <ThemeProvider>
      <Header />
    </ThemeProvider>
  );
  expect(container.innerHTML).toMatch('Im Hungry');
});

test('Header changes theme button click', () => {
  const { getByTestId } = render(
    <ThemeProvider>
      <Header />
    </ThemeProvider>
  );

  expect(getByTestId('current-theme').innerHTML).toMatch('relaxed');
  fireEvent.click(getByTestId('cloudy-theme-button'));
  expect(getByTestId('current-theme').innerHTML).toMatch('cloudy');
});
